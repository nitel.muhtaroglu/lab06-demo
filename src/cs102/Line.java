package cs102;

public class Line {
    private Point start;
    private Point finish;

    public Line(Point start, Point finish) {
        this.start = start;
        this.finish = finish;
    }
}
