package cs102;

public class Main {

    public static void main(String[] args) {
        Point point1 = new Point(0, 0);
        Point point2 = new Point(200, 200);
        Line line = new Line(point1, point2); //(0, 0) ---> (200, 200) changed to (8, 0) ---> (200, 200)
        int x = 8;
        point1.setX(x);
        point1.setPoint(point2);
    }
}
